const path = require('path');
const http = require('http');
const express = require('express');
const socketIO = require('socket.io');

const publicPath = path.join(__dirname, '../public');
const port = process.env.PORT || 5000;
var app = express();
var server = http.createServer(app);
var io = socketIO(server);

app.use(express.static(publicPath));

io.on('connection', (socket) => {
    console.log('User connected');
    socket.on('notification', (message) => {
        console.log(`Receive ${message}`);
        io.emit('notification', 'OK');
    });
});

server.listen(port, () => {
    console.log(`Server started port ${port}.`);
});